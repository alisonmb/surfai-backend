'use strict';
var app = require('../../server/server');

module.exports = function(Basemodel) {

  var mongoDs = app.datasources.db;
  var ObjectID = mongoDs.connector.getDefaultIdType();

  Basemodel.defineProperty('createdBy', {
    type: ObjectID,
  });

  Basemodel.defineProperty('updatedBy', {
    type: ObjectID,
  });

};
