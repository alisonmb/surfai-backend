'use strict';
const randtoken = require('rand-token').generator({chars: 'numeric'});
const accountNoPrefix = require('rand-token').generator({chars: 'ALPHA'});
const accountNoHexa = require('rand-token').generator({chars: '0123456789ABCDEF'});

const async = require('async');
const _ = require('lodash');

// const fromNumber = '+14158002269';

module.exports = Account => {

  // Account.validatesUniquenessOf('accountNo',  {message: 'Account number already assigned to other user'});

  // Account.confirmCode = ({email, code}, next) => {
  //   let account;
  //   const {Role, RoleMapping} = Account.app.models;
  //
  //   if(!email || !code) {
  //     console.log("Invalid params!");
  //     return next({
  //       statusCode: 422,
  //       message: "Invalid params",
  //       name: "Error",
  //       code: "CONFIRMATION_FAILED"
  //     });
  //   }
  //
  //   async.waterfall([
  //
  //     cb => {
  //       Account.findOne({where: {email: email}, include: ['verificationCode']}, cb);
  //     },
  //
  //     (result, cb) => {
  //       if (!result) {
  //         cb({
  //           statusCode: 404,
  //           message: "User not found",
  //           name: "Error",
  //           code: "CONFIRMATION_FAILED"
  //         });
  //       } else {
  //         account = result;
  //
  //         if (account.codeVerified) {
  //           cb({
  //             statusCode: 400,
  //             message: "This account has been already confirmed.",
  //             name: "Error",
  //             code: "ACCOUNT_ALREADY_CONFIRMED"
  //           });
  //         } else if (code != account.toJSON().verificationCode.code) {
  //           cb({
  //             statusCode: 401,
  //             message: "Invalid confirmation code",
  //             name: "Error",
  //             code: "CONFIRMATION_FAILED"
  //           });
  //         } else {
  //           account.updateAttribute('codeVerified', new Date(), cb);
  //         }
  //
  //       }
  //     },
  //
  //     (result, cb) => {
  //       //find the User role instance
  //       Role.findOne({name: 'User'}, cb);
  //     },
  //
  //     (result, cb) => {
  //       //assigns the USER role to this account
  //       result.principals.create({
  //         principalType: RoleMapping.USER,
  //         principalId: account.id
  //       }, cb);
  //     }
  //
  //   ], (err, result) => {
  //     if(err) {
  //       console.log("error ", err);
  //       next(err);
  //     } else {
  //       account = _.omit(account, ['verificationCode', 'codeVerified', 'dateOfTermsAcceptance']);
  //       next(null, account);
  //     }
  //
  //     //avoid mem leak
  //     account = null;
  //   });
  // };
  //

  // Account.afterRemote('login', function(ctx, accessToken, next) {
  //   console.log('accessToken', accessToken);
  //   if (accessToken && accessToken.id) {
  //     Account.findById(accessToken.userId, (err, result) => {
  //       if(err || !result) {
  //         return next({
  //           statusCode: 500,
  //           message: "Error checking the account verification",
  //           name: "Error",
  //           code: "LOGIN_VERIFICATION_SERVER_ERROR"
  //         })
  //       }
  //
  //     });
  //   }
  // });



  // Account.afterRemote('create', (context, account, next) => {
  //   const {Twilio, VerificationCode} = Account.app.models;
  //   const token = randtoken.generate(4);
  //   let verifCode;
  //
  //   //TODO: implement async.waterfall
  //
  //   async.waterfall([
  //
  //     //1. creates the verification code for this account
  //     cb => {
  //       VerificationCode.create({
  //         "code": token,
  //         "phoneNumber": account.phoneNumber,
  //         "accountId": account.id,
  //         "dateSent": new Date()
  //       }, cb);
  //     },
  //
  //     //2. sends the verification code to the phoneNumber
  //     (result, cb) => {
  //       verifCode = result;
  //       Twilio.send({
  //         type: 'sms',
  //         to: account.phoneNumber,
  //         from: fromNumber,
  //         body: 'Your KNOW security code is ' + verifCode.code,
  //       }, cb);
  //     },
  //
  //     //3. saves twilio return log to the verification object
  //     (result, cb) => {
  //       verifCode.providerResponse = result;
  //       verifCode.save(cb);
  //     }
  //
  //   ], (err, result) => {
  //     // 4a. in case of any error, delete the created user and verification code request
  //     if (err) {
  //       console.log("ERROR: ", err);
  //       if (account) Account.deleteById(account.id);
  //       if (verifCode) VerificationCode.deleteById(verifCode.id);
  //
  //       //5a. returns the error to the request chain
  //       next(err);
  //     } else {
  //       //4b. returns to the request chain
  //       next();
  //     }
  //   });
  // });


};
