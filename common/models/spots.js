'use strict';
var loopback = require('loopback');

module.exports = function(Spots) {

    Spots.nearestSpots = function (lat, lng, cb) {

        let userLocation;

        if (lat || lng)
            userLocation = new loopback.GeoPoint({lat: lat, lng: lng});

        Spots.find({where: {location: {near: userLocation}}, limit: 5}, function(err, near) {
            cb(err, near);
        });

    };
    Spots.remoteMethod('nearestSpots', {
        http: { path: '/nearestSpots', verb: 'get' },
        accepts: [{arg: 'lat', type: 'number'}, {arg: 'lng', type: 'number'}],
        returns: { type: 'object', root: true }
    });

};
