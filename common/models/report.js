'use strict';
var app = require('../../server/server');
var loopback = require('loopback');
const googleMapsClient = require('@google/maps').createClient({key:'AIzaSyA1_idbO82vdNz7s-wasPySYfesEkRK3Po'});


module.exports = function(Report) {


    Report.nearestReport = function (lat, lng, cb) {
        var d = new Date();
        d.setDate(d.getDate() - 1);
        d.setHours(0,0,0,0);

        let userLocation;
        if (lat && lng) {
            userLocation = new loopback.GeoPoint({lat: lat, lng: lng});
            Report.find({where: {and: [ {location: {near: userLocation, maxDistance: 10000000, unit: 'meters'}}, {createdAt: {gt: d.toISOString()}}]}}, cb);
        } else {
            Report.find({where: {createdAt: {gt: d.toISOString()}}, order: 'createdAt DESC'}, cb);
        }


    };
    Report.remoteMethod('nearestReport', {
        http: { path: '/nearestReport', verb: 'get' },
        accepts: [{arg: 'lat', type: 'number'}, {arg: 'lng', type: 'number'}],
        returns: { type: 'object', root: true }
    });

    Report.beforeRemote('create', (context, report, next) => {

        if (context.args.data.location &&
            context.args.data.location.lat &&
            context.args.data.location.lng &&
            context.args.data.location.lat === 0 &&
            context.args.data.location.lng === 0) {
            const city = context.args.data.city ? context.args.data.city : '';
            const state = context.args.data.state ? context.args.data.state : '';
            const search = context.args.data.spot + ' ' + city + ' ' + state;

            googleMapsClient.geocode({address: search}, function(err, response) {
                if (!err) {
                    context.args.data.location = response.json.results[0].geometry.location;
                    next();
                } else {
                    next();
                }
            });
        } else {
            next();
        }

    });

    Report.afterRemote('create', (context, report, next) => {
        const Spots = app.models.Spots;

        // Adiciona novo spot
        Spots.findOne({where: {'spot_name': report.spot}}, function (err, data) {
            if (data === null) {
                Spots.create({'spot_name': report.spot, 'location': report.location, 'rating': 1});
            } else {
                data.rating = data.rating + 1;
                data.save();
            }
            next();
        });

    });
};





