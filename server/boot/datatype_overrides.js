module.exports = function(app){
  var mongoDs = app.datasources.db;
  var ObjectID = mongoDs.connector.getDefaultIdType();
  var { RoleMapping } = app.models;

  RoleMapping.defineProperty('principalId', {
    type: ObjectID,
  });

};
